

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;


public class EncryptionDecryptionString {

    private static final String UNICODE_FORMAT = "UTF-8";

    public static void main(String[] args) {

        String text = "Hejsan Olle";


        try {
            SecretKey key = generateKey("AES");
            Cipher cipher2;
            cipher2 = Cipher.getInstance("AES");

            byte[] encryptedData = encryptionString(text,key,cipher2);
            String encryptedString = new String(encryptedData);
            System.out.println(encryptedString);
            String decrypted = decryptionString(encryptedData,key,cipher2);
            System.out.println(decrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static SecretKey generateKey(String encryptionType) {

        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(encryptionType);
            SecretKey myKey = keyGenerator.generateKey();
            return myKey;
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] encryptionString(String dataToEncrypt, SecretKey myKey,Cipher cipher) {


        try {
            byte[] text = dataToEncrypt.getBytes(UNICODE_FORMAT);
            cipher.init(Cipher.ENCRYPT_MODE,myKey);
            byte[] textEncrypted = cipher.doFinal(text);
            return textEncrypted;

        } catch (Exception e) {
            return null;
        }
    }

    public static String decryptionString(byte[] dataToDecrypt, SecretKey myKey, Cipher cipher) {


        try {
            cipher.init(Cipher.DECRYPT_MODE,myKey);
            byte[] textDecrypted = cipher.doFinal(dataToDecrypt);
            String result = new String(textDecrypted);
            return result;
        } catch (Exception e) {
            return null;
        }
    }


}
